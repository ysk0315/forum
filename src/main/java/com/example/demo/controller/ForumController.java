package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		//form用の空のentityを準備
		Report report = new Report();
		//画面遷移先を指定
		mav.setViewName("/new");
		//準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	//投稿内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		//form用の空のentityを準備(Comment)
		Comment comment = new Comment();
		//投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		//返信を全件取得
		List<Comment> messageData = commentService.findAllComment();
		//List<Comment>
		//画面遷移先を指定
		mav.setViewName("/top");
		//準備した空のentity(Comment)を保管
		mav.addObject("formModel", comment);
		//投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		//返信データオブジェクトを保管
		mav.addObject("messages", messageData);
		return mav;
	}

	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id);
		mav.addObject("formModel", report);
		mav.setViewName("/edit");
		return mav;
	}

	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("forumModel") Report report) {
		report.setId(id);
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		//投稿をテーブルに格納
		reportService.saveReport(report);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}
}
