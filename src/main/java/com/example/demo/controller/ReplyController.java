package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class ReplyController {

	@Autowired
	CommentService commentService;

	@PostMapping("/reply")
	public ModelAndView addMessage(@ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	@GetMapping("/edit/reply/{id}")
	public ModelAndView editMessage(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id);
		mav.addObject("formModel", comment);
		mav.setViewName("/edit/reply");
		return mav;
	}

	@PutMapping("/update/reply/{id}")
	public ModelAndView updateMessage(@PathVariable Integer id, @ModelAttribute("forumModel") Comment comment) {
		comment.setId(id);
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/delete/reply/{id}")
	public ModelAndView deleteMessage(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
