package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	public List<Report> findAllReport(){
		return reportRepository.findAll();
	}

	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	public void deleteReport(int id) {
		reportRepository.deleteById(id);
	}
}
